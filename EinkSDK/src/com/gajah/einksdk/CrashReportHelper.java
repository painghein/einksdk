package com.gajah.einksdk;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import android.app.Application;

@ReportsCrashes(formKey = "", // will not be used
formUri = "http://test1.oaxisbooks.com/EinkLog/catcher.php", mode = ReportingInteractionMode.SILENT)
public class CrashReportHelper {
	public void init(Application app) {
		ACRA.init(this.getClass().getAnnotation(ReportsCrashes.class), app);
	}
}
