package com.gajah.einksdk;

import android.os.Build;
import android.view.KeyEvent;

public class EinkKeyEvent {
	public static int KEYCODE_PAGEUP = KEYCODE_PAGEUP();

	public static int KEYCODE_PAGEDOWN = KEYCODE_PAGEDOWN();

	private static int KEYCODE_PAGEUP() {
		if (KEYCODE_PAGEUP == 0) {
			if (Build.MODEL.equalsIgnoreCase("bk6007")) {
				KEYCODE_PAGEUP = BK6007.KEYCODE_PAGEUP;
			} else if (Build.MODEL.equalsIgnoreCase("bk6003")) {
				KEYCODE_PAGEUP = BK6003.KEYCODE_PAGEUP;
			} else if (Build.MODEL.equalsIgnoreCase("bk6005")) {
				KEYCODE_PAGEUP = BK6003.KEYCODE_PAGEUP;
			} else if (Build.MODEL.equalsIgnoreCase("bk6004")) {
				KEYCODE_PAGEUP = KeyEvent.KEYCODE_PAGE_UP;
			} else {
				KEYCODE_PAGEUP = KeyEvent.KEYCODE_VOLUME_DOWN;
			}
		}
		return KEYCODE_PAGEUP;
	}

	private static int KEYCODE_PAGEDOWN() {
		if (KEYCODE_PAGEDOWN == 0) {
			if (Build.MODEL.equalsIgnoreCase("bk6007")) {
				KEYCODE_PAGEDOWN = BK6007.KEYCODE_PAGEDOWN;
			} else if (Build.MODEL.equalsIgnoreCase("bk6003")) {
				KEYCODE_PAGEDOWN = BK6003.KEYCODE_PAGEDOWN;
			} else if (Build.MODEL.equalsIgnoreCase("bk6005")) {
				KEYCODE_PAGEDOWN = BK6003.KEYCODE_PAGEDOWN;
			} else if (Build.MODEL.equalsIgnoreCase("bk6004")) {
				KEYCODE_PAGEDOWN = KeyEvent.KEYCODE_PAGE_DOWN;
			} else {
				KEYCODE_PAGEDOWN = KeyEvent.KEYCODE_VOLUME_UP;
			}
		}
		return KEYCODE_PAGEDOWN;
	}
}
